// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false ,

  firebaseConfig : {
    apiKey: "AIzaSyDSWjkaVdo90-er785SI97n1HC_MAPCsW0",
    authDomain: "home-test-example.firebaseapp.com",
    databaseURL: "https://home-test-example.firebaseio.com",
    projectId: "home-test-example",
    storageBucket: "home-test-example.appspot.com",
    messagingSenderId: "820683618891",
    appId: "1:820683618891:web:12d7df0245a0e92a1e6bce"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
