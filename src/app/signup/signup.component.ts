import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(public authService:AuthService, private router:Router) { }

  ngOnInit() {
  }

  SignUp(name, pass){
    this.authService.SignUp(name, pass);
  }

}
