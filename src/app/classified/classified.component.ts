import { DBuser } from './../Dbuser';
import { AuthService } from './../auth.service';
import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { ImageService } from '../image.service';

export interface Classify {
  value: string;
  viewValue: string;
}


@Component({
  selector: 'app-classified',
  templateUrl: './classified.component.html',
  styleUrls: ['./classified.component.css']
})
export class ClassifiedComponent implements OnInit {

  classResult:string;

  classify:Classify[] =[
    {value: '0', viewValue: 'business'},
    {value: '1', viewValue: 'entertainment'},
    {value: '2', viewValue: 'politics'},
    {value: '3', viewValue: 'sport'},
    {value: '4', viewValue: 'tech'},
  ];

  add(){
    this.classifyService.addToFirebase(this.auth.getUser().uid,this.classifyService.doc,this.classResult); 
  }

  category:string = "Loading...";
  categoryImage:string; 
  constructor(public classifyService:ClassifyService,
              public imageService:ImageService,public auth:AuthService) {}

  ngOnInit() {
    // console.log(this.auth.isLoggedIn)
    // console.log(this.auth.getUser().uid)
    this.classifyService.classify().subscribe(
      res => {
        // console.log(res);
        // console.log(this.classifyService.categories[res])
        this.category = this.classifyService.categories[res];
        // console.log(this.imageService.images[res]);
        this.categoryImage = this.imageService.images[res];
        // console.log(this.classifyService.doc)
      }
    )
  }
}
